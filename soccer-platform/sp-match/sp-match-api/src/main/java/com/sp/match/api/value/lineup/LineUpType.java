package com.sp.match.api.value.lineup;

public enum LineUpType {
	STARTER, SUBSTITUTION
}
