package com.sp.match.api.value;

public enum WinnerType {
	HOME, AWAY, DRAW;
}
