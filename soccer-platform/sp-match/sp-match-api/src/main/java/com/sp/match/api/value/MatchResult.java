package com.sp.match.api.value;

public enum MatchResult {
	WIN, LOSE, DRAW
}
