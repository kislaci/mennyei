import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import './style/style.less';

platformBrowserDynamic().bootstrapModule(AppModule);
